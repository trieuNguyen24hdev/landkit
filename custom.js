// Open/close btn
const openNavMenuBtn = document.querySelector(".humberger-btn");
const closeNavMenuBtn = document.querySelector(".close-btn");
const navMenu = document.querySelector(".menu-toggle");
openNavMenuBtn.addEventListener("click", () => {
  navMenu.classList.add("show");
});
closeNavMenuBtn.addEventListener("click", () => {
  navMenu.classList.remove("show");
});

// Validate

function Validator(options) {

  // Hàm thực hiện Validate
  function validate(inputElement, rule) {
    var errorElement = inputElement.parentElement.querySelector(options.errorSelector);
    var errorMessage = rule.test(inputElement.value);

            // Lấy ra rules của selector
            // var rules = selectorRule[rule.selector];

            // Lặp qua từng rules & kiểm tra
            // Nếu có lỗi thì dừng kiểm tra
            // for (var i = 0; i < rules.length; ++i) {
            //     errorMessage = rule[i] (inputElement.value);
            //     if (errorMessage) break;
            // }

            if (errorMessage) {
                errorElement.innerText = errorMessage;
                inputElement.parentElement.classList.add('invalid')
            } else {
              errorElement.innerText = '';
              inputElement.parentElement.classList.remove('invalid')
            }

            return !errorMessage;
  }

  // Lấy element của form cần validate
  var formElement = document.querySelector(options.form);
  if (formElement) {
    // Khi submit form
    formElement.onsubmit = function (e) {
      e.preventDefault();
      var isFormValid = true;
      // Thực hiện lặp qua từng rules và validate
      options.rules.forEach(function (rule) {
        var inputElement = formElement.querySelector(rule.selector);
        var isValid = validate(inputElement, rule);
        if (!isValid) {
          isFormValid = false;
        }
      });

      if (isFormValid) {
        if (typeof options.onSubmit === 'function') { 
          var enableInputs = formElement.querySelectorAll('[name]');
          var formValues = Array.from(enableInputs).reduce(function (value, input) {
            return (value[input.name] = input.value) && value;
          }, {});
          options.onSubmit(formValues);
        }
      }
    }

    // Lặp qua mỗi rule và xử lý
    options.rules.forEach(function (rule) {
      var inputElement = formElement.querySelector(rule.selector)
      if (inputElement) {
        // Xử lý trường hợp blur ra khỏi input
          inputElement.onblur = function () {
            validate(inputElement, rule);
          }
        // Xử lý mỗi khi người dùng nhập vào input
          inputElement.oninput = function () {
            var errorElement = inputElement.parentElement.querySelector(options.errorSelector);
            errorElement.innerText = '';
            inputElement.parentElement.classList.remove('invalid');
        }
      }
    });
  }
}

Validator.isRequired = function(selector) {
  return {
    selector: selector,
    test: function (value) {
      return value.trim() ? undefined : 'Vui lòng nhập tên đăng nhập'      
    }
  };
}

Validator.isEmail = function(selector) {
  return {
    selector: selector,
    test: function (value) {
      var regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      return regex.test(value) ? undefined : 'Vui lòng nhập đúng email'
    }
  };
}

Validator.minLength = function(selector, min) {
  return {
    selector: selector,
    test: function (value) {
      return value.length >= min ? undefined : 'Vui lòng nhập tối thiểu 8 kí tự';
    }
  };
}

// Slider

var slideIndex = 1;
showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
  showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("slider");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  slides[slideIndex-1].style.display = "flex";
}

const prev = document.querySelector(".prev");
const next = document.querySelector(".next");
const textSlides = document.querySelectorAll(".text-slide");


next.addEventListener("click",()=>{
  for(let i=0; i< textSlides.length; i++){
      if(textSlides[i].classList.contains("show")){
          textSlides[i].classList.remove("show");
          textSlides[i].style="animation: switch-left-hidden 0.4s ease";          
      }else{
          textSlides[i].classList.add("show");
          textSlides[i].style="animation: switch-left-show 0.4s ease";
      }
  }
});
prev.addEventListener("click",()=>{
  for(let i=0; i< textSlides.length; i++){
      if(textSlides[i].classList.contains("show")){
          textSlides[i].classList.remove("show");
          textSlides[i].style="animation: switch-right-hidden 0.4s ease";
          
      }else{
          textSlides[i].classList.add("show");
          textSlides[i].style="animation: switch-right-show 0.4s ease";    
      }
  }
});

